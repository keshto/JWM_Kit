#!/usr/bin/python3
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
import gi, os, re
import xml.etree.ElementTree as ET
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GdkPixbuf

# JWM Kit - A set of Graphical Apps to simplify use of JWM (Joe's Window Manager) <https://codeberg.org/JWMKit/JWM_Kit>
# Copyright ©  2020-2022 Calvin Kent McNabb <apps.jwmkit@gmail.com>
#
# This file is part of JWM Kit.
#
# JWM Kit is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License, version 2,
# as published by the Free Software Foundation.
#
# JWM Kit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with JWM Kit.  If not, see <https://www.gnu.org/licenses/>.


def create_tray_svg(style, name):
    name = '{}/.config/jwm/themes/{}-g-tray.svg'.format(os.path.expanduser('~'), name[:-6])
    svg = '<?xml version="1.0" encoding="UTF-8"?>' \
          '<svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="36" width="148">\n'
    if ':' in style[2]:
        tmp = style[2].split(':')
        svg = '{} <defs>\n  <linearGradient id="grad1" x1="0%" y1="0%" x2="0%" y2="100%">\n' \
              '   <stop offset="0%" style="stop-color:{}" />\n   <stop offset="100%" style="stop-color:{}" />\n' \
              '  </linearGradient>\n </defs>\n'.format(svg, tmp[0], tmp[1])
        style[2] = 'url(#grad1)'
    svg = '{} <rect style="fill:{};stroke:{};stroke-width:2" width="148" height="36" />' \
          '\n <rect style="fill:#BFBFBF;stroke:#000000;stroke-width:1" width="22" height="20" x="120" y="8"' \
          ' rx="1.5"/>'.format(svg, style[2], style[3])
    svg = '{}\n <rect style="fill:#0062C5" width="22" height="4" x="120" y="8" rx="1.5"/>'.format(svg)
    svg = '{}\n <rect style="fill:none;stroke:#000000;stroke-width:1" width="14" height="12" x="120" y="16" rx="1.5"/' \
          '>\n <rect style="fill:#0062C5" width="14" height="3" x="120" y="16" rx="1.5"/>\n ' \
          '<rect style="fill:#000000;stroke:#BFBFBF;stroke-width:1" width="22" height="20" x="88" y="8" rx="1.5"/>' \
          '\n <path style="stroke:#BFBFBF;stroke-width:1" d="M 94 15 98 17" />\n ' \
          '<path style="stroke:#BFBFBF;stroke-width:1" d="M 94 15 98 13" />\n ' \
          '<path style="stroke:#BFBFBF;stroke-width:1" d="M 100 17 105 17" />\n ' \
          '<rect style="fill:#BFBFBF;stroke:#000000;stroke-width:1" width="22" height="20" x="56" y="8" rx="1.5"/>\n ' \
          '<rect style="fill:#0062C5" width="22" height="4" x="56" y="8" rx="1.5"/>\n ' \
          '<path style="stroke:#B68B00;stroke-width:5" d="M 62 24 73 14" />\n ' \
          '<path style="stroke:#0062C5;stroke-width:5" d="M 71 16 73 14" />\n ' \
          '<path style="fill:#4e4e4e" d="m 59,27 1.232393,-4.956446 3.736646,3.707416 z"/>'.format(svg)
    if style[0] != '':
        if style[0][2]:
            style[0][2] = '{} '.format(style[0][2])
        svg = '{}\n <style>.single {{ font: {}{}px {}; }}</style>'.format(svg, style[0][2], style[0][1],
                                                                         style[0][0].replace('.', ' '))
        svg = '{}\n <text x="7" y="22" class="single" fill="{}">Menu</text>'.format(svg, style[1])
    svg = '{}\n</svg>'.format(svg)
    with open(name, 'w+') as f:
        f.write(svg)


def create_win_svg(style, name, active):
    if not style[3].isdigit():
        style[3] = '4'
    name = '{}/.config/jwm/themes/{}-g-{}.svg'.format(os.path.expanduser('~'), name[:-6], active)
    svg = '<?xml version="1.0" encoding="UTF-8"?>\n' \
          '<svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="22" width="73">'
    if ':' in style[1]:
        tmp = style[1].split(':')
        svg = '{}\n <defs>\n  <linearGradient id="grad1" x1="0%" y1="0%" x2="0%" y2="100%">\n' \
              '   <stop offset="0%" style="stop-color:{}" />\n   <stop offset="100%" style="stop-color:{}" />\n' \
              '  </linearGradient>\n </defs>'.format(svg, tmp[0], tmp[1])
        style[1] = 'url(#grad1)'
    svg = '{}\n <rect style="fill:{};stroke:{};stroke-width:1" width="73" height="26" rx="{}" />'.format(svg, style[1],
                                                                                                 style[2], style[3])
    svg = '{}\n <path style="fill:none;stroke:{};stroke-width:2" d="M 65 7 55 16 ' \
          'M 55 7 65 16 M 10 15 21 15 M44 7 44 16 32 16 32 7z M 32 8 44 8" /></svg>'.format(svg, style[0])
    svg = svg.replace('\n\n', '\n')
    with open(name, 'w+') as f:
        f.write(svg)


def get_about(button, self,):
    about_window = AboutWindow()
    about_window.connect("delete-event", Gtk.main_quit)
    about_window.set_transient_for(self)
    about_window.show_all()
    self.set_sensitive(False)
    Gtk.main()
    self.set_sensitive(True)


def ok_action(button):
    os.system('jwmkit_first_run')
    exit()


def app_fail():
    window = AppFail()
    window.connect("delete-event", Gtk.main_quit)
    window.set_position(Gtk.WindowPosition.CENTER)
    window.show_all()
    Gtk.main()


def get_jwmrc():
    # determine default location of jwmrc based on version and configuration
    home = os.path.expanduser('~')
    version = os.popen('jwm -v').read()
    version = version[5:8]
    try:
        version = float(version)
    except ValueError:
        # assume version 2.3 if the version number can not be converted to a float
        version = 2.3
    if version < 2.4:
        version = "{}/.jwmrc".format(home)
    else:
        version = "{}/.config/jwm/jwmrc".format(home)
        if not os.path.isfile(version):
            version = "{}/.jwmrc".format(home)
    return version


def create_image(image, h, w, a):
    try:
        pb = GdkPixbuf.Pixbuf.new_from_file_at_scale(image, h, w, preserve_aspect_ratio=a)
    except gi.repository.GLib.Error:
        pb = GdkPixbuf.Pixbuf.new_from_file_at_scale('/usr/share/pixmaps/jwmkit/transparent.png', h, w, preserve_aspect_ratio=a)
    image = Gtk.Image()
    image.set_from_pixbuf(pb)
    return image


def define_tools(button, win):
    dialog = Gtk.Dialog("Define tools", win, 0)
    box = dialog.get_content_area()
    dialog.add_buttons( Gtk.STOCK_SAVE, Gtk.ResponseType.OK, Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)
    dialog.set_default_size(370, -1)
    box.set_border_width(10)
    box.set_spacing(10)
    label = Gtk.Label(label="Define Tools")
    box.add(label)

    row = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
    txt_entry = Gtk.Entry()
    txt_entry.set_text(win.helpers[2])
    txt_entry.set_property("width-request", 260)
    row.pack_end(txt_entry, False, False, 0)
    row.pack_end(Gtk.Label(label='Text Editor ', xalign=0), False, False, 0)
    box.add(row)

    row = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
    arhive_entry = Gtk.Entry()
    arhive_entry.set_text(win.helpers[3])
    arhive_entry.set_property("width-request", 260)
    row.pack_end(arhive_entry, False, False, 0)
    row.pack_end(Gtk.Label(label='Archive ', xalign=0), False, False, 0)
    box.add(row)

    row = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
    fm_entry = Gtk.Entry()
    fm_entry.set_text(win.helpers[4])
    fm_entry.set_property("width-request", 260)
    row.pack_end(fm_entry, False, False, 0)
    row.pack_end(Gtk.Label(label='File Manager ', xalign=0), False, False, 0)
    box.add(row)
    dialog.show_all()
    response = dialog.run()

    if response == -5:
        win.helpers[2] = txt_entry.get_text()
        win.helpers[3] = arhive_entry.get_text()
        win.helpers[4] = fm_entry.get_text()
        output = 'logout={}\ntime={}\neditor={}\narchiver={}\nfilemanager={}\n'\
            .format(win.helpers[0], win.helpers[1], win.helpers[2], win.helpers[3], win.helpers[4])
        with open('{}/.config/jwmkit/setting_su'.format(os.path.expanduser('~')), "w+") as f:
            f.write(output)

        editor = txt_entry.get_text()
        dialog.destroy()
        return editor
    else:
        dialog.destroy()


def read_setting_su():
    path = os.path.expanduser("~")
    path = '{}/.config/jwmkit/setting_su'.format(path)
    values = []
    # Read the file
    if os.path.isfile(path):
        with open(path) as f:
            data = f.read()
        # search file for values
        for value in ('logout', 'time', 'editor', 'archiver', 'filemanager'):
            try:
                values.append(re.findall('(?<![#]){}=(.*)'.format(value), data)[0])
            except IndexError:
                values.append('')

        # convert alias parameters
        if values[0].lower() in ('systemd', 'systemctl', 'timedatectl'):
            values[0] = 'sysd'
        elif values[0].lower() in ('dbus', 'consolekit'):
            values[0] = ''
        elif values[0].lower() == 'askpass':
            values[0] = 'sudoa'
        elif values[0].lower() == 'su-to-root -c':
            values[0] = 'nox'
        elif values[0].lower() == 'su-to-root -x -c':
            values[0] = 'x'
        if values[1].lower() in ('systemd', 'systemctl', 'timedatectl'):
            values[1] = 'sysd'
        elif values[1].lower() == 'askpass':
            values[1] = 'sudoa'
        elif values[1].lower() == 'su-to-root -c':
            values[1] = 'nox'
        elif values[1].lower() == 'su-to-root -x -c':
            values[1] = 'x'
        return values
    else:
        return ['', '', '', '', '']


def private_entry(win):
    answer = [True, False]

    def enter_key(entry):
        answer[1] = entry.get_text()
        dialog.destroy()

    dialog = Gtk.Dialog("Password Prompt", win, 0)
    box = dialog.get_content_area()
    dialog.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK)
    dialog.set_default_size(370, -1)
    box.set_border_width(10)
    box.set_spacing(10)
    label = Gtk.Label(label="Enter your administrative password")
    entry = Gtk.Entry()
    entry.set_visibility(False)
    entry.connect("activate", enter_key)
    box.add(label)
    box.add(entry)
    dialog.show_all()
    response = dialog.run()
    if response == -6:
        dialog.destroy()
        return False, False
    elif response == -5:
        pw = entry.get_text()
        dialog.destroy()
        return True, pw
    else:
        dialog.destroy()
        return answer


def warning_settings(self):
    messages = ["<big><b>!!! Warning !!!</b></big>", "Settings file is missing, incomplete or damaged\n",
                "Use JWM Kit Repair &amp; Restore to repair the settings file",
                "...or you can do it manually if you like\n"]
    main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=5)
    self.add(main_box)
    for message in messages:
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
        main_box.add(box)
        label = Gtk.Label()
        label.set_markup(message)
        box.add(label)
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
        main_box.add(box)
    ok_button = Gtk.Button(label='OK', image=Gtk.Image(stock=Gtk.STOCK_OK))
    ok_button.connect("clicked", Gtk.main_quit)
    box.pack_end(ok_button, False, False, 15)
    return


def get_su(tool):
    setting_su = '{}/.config/jwmkit/setting_su'.format(os.path.expanduser('~'))
    if os.path.isfile(setting_su):
        with open(setting_su) as f:
            su_data = f.read()
        try:
            return re.findall('{}=(.*)'.format(tool), su_data)[0]
        except IndexError:
            return


def get_paths():
    home = os.path.expanduser('~')
    jwmrc = get_jwmrc()
    tray_list = []
    try:
        tree = ET.parse(jwmrc)
    except Exception as exception_message:
        print(str(exception_message))
        app_fail()
    root = tree.getroot()
    tmp = []
    for node in root:
        if node.tag == 'Include':
            node = node.text
            if node.startswith('$HOME'):
                node = '{}{}'.format(home, node[5:])
            if os.path.isfile(node):
                tmp.append(node)
    return tmp


def find_terminal():
    # find a terminal. Sadly Linux has no unified spec for setting a default terminal
    def pup_default():
        if os.path.isfile('/usr/local/bin/defaultterminal'):
            with open('/usr/local/bin/defaultterminal') as f:
                data = f.read()
            try:
                if re.findall('exec ([^ "]+)', data)[0]:
                    return True
            except IndexError:
                return False
    if pup_default():
        term = 'defaultterminal'
    elif (os.path.isfile('/etc/alternatives/x-terminal-emulator')
            and os.path.isfile('/usr/bin/x-terminal-emulator')):
        term = 'x-terminal-emulator'
    else:
        terms = ['xfce4-terminal', 'i3-sensible-terminal', 'lxterminal', 'gnome-terminal', 'roxterm',
                 'mate-terminal', 'Eterm', 'terminology', 'sakura', 'terminator',
                 'qterminal', 'deepin-terminal', 'xterm', 'rxvt']
        term = [term for term in terms if os.path.isfile('/usr/bin/' + term)]
        try:
            term = term[0]
        except IndexError:
            # not terminal found. Using xterm. The user may have to manually edit command for terminal apps.
            term = 'xterm'
    if term:
        return term


def theme_test(theme_path):
    theme_tags = ['WindowStyle', 'ClockStyle', 'TrayStyle', 'TaskListStyle', 'TrayButtonStyle', 'PagerStyle',
                  'MenuStyle', 'PopupStyle', 'ButtonClose', 'ButtonMax', 'ButtonMaxActive', 'ButtonMenu',
                  'ButtonMin', 'DefaultIcon']
    if theme_path.startswith('$HOME'):
        theme_path = os.path.expanduser('~') + theme_path[5:]
    if os.path.isfile(theme_path):
        try:
            tree = ET.parse(theme_path)
            root = tree.getroot()
        except Exception as exception_message:
            print('{} in {}'.format(str(exception_message)))
            return False
        if root[0].tag in theme_tags:
            return 'theme'


def id_path(path):
    values = ['start', 'icon', 'group', 'theme', 'keys', 'preferences', 'menu', 'tray']
    test_tags = [['StartupCommand', 'RestartCommand', 'ShutdownCommand'], ['IconPath'], ['Group'],
                 ['Include'], ['Key', 'Mouse'],
                 ['Desktops', 'DoubleClickSpeed', 'DoubleClickDelta', 'FocusModel', 'MoveMode',
                  'ResizeMode', 'SnapMode'], ['RootMenu'], ['Tray']]
    if path[-19:] == '.config/jwm/buttons':
        return 'buttons'
    try:
        tree = ET.parse(path)
        root = tree.getroot()
        if root.tag != 'JWM':
            print('Expected outer tag to be JWM in {}'.format(path))
            return False
        elif len(root) == 0:
            return "empty"
        else:
            for i, tags in enumerate(test_tags):
                if root[0].tag in tags:
                    # test if the included file is a theme
                    if root[0].text == 'exec:jwmkit_buttons':
                        return 'buttons'
                    if root[0].tag == 'Include':
                        return(theme_test(root[0].text))
                    return values[i]
        return False
    except Exception as exception_message:
        print('{} in {}'.format(str(exception_message), path))
        return False


def find_configs():
    start, icon, group, theme, keys, preferences, menu, tray, buttons, empty = [], [], [], [], [], [], [], [], [], []
    unknown = []
    configs = [start, icon, group, theme, keys, preferences, menu, tray, empty]
    values = ['start', 'icon', 'group', 'theme', 'keys', 'preferences', 'menu', 'tray', 'buttons', 'empty']
    path_ids = []
    paths = get_paths()

    for path in paths:
        path_ids.append(id_path(path))
    for id, path, in zip(path_ids, paths):
        try:
            configs[values.index(id)].append(path)
        except (ValueError, IndexError):
            unknown.append(path)
    configs.append(unknown)
    return configs


def get_applications_data():
    # Gather data on installed apps.
    term = find_terminal()
    sys_appdir = '/usr/share/applications'
    home_appdir = os.path.join(os.path.expanduser('~'), '.local/share/applications')
    if not os.path.isdir(home_appdir):
        os.makedirs(home_appdir)
    applications = sorted(os.listdir(sys_appdir), key=str.lower)
    sys_applications = re.findall("'([^']+?desktop)'", str(applications))
    applications = sorted(os.listdir(home_appdir), key=str.lower)
    home_applications = re.findall("'([^']+?desktop)'", str(applications))
    sys_applications = [i for i in sys_applications if i not in home_applications]
    sys_applications = [os.path.join(sys_appdir, i) for i in sys_applications]
    home_applications = [os.path.join(home_appdir, i) for i in home_applications]
    applications = sys_applications + home_applications
    lang = os.getenv('LANG')
    locale = re.split('[_ .]', lang, 2)
    lang = '[{}]'.format(locale[0])
    locale = '[{}_{}]'.format(locale[0], locale[1])
    search_keys = [['Name{}'.format(re.escape(locale)), 'Name{}'.format(re.escape(lang)), 'Name'],
                   ['Comment{}'.format(re.escape(locale)), 'Comment{}'.format(re.escape(lang)), 'Comment'],
                   'Exec', 'Icon', 'Terminal']
    alldata = []
    for application in applications:
        with open(application) as f:
            appdata = f.read()
        appdata = re.findall('(?s)(\[Desktop Entry\].+?(?=\[Desktop|\Z))', appdata, re.MULTILINE)[0]
        data = []
        for key in search_keys:
            if type(key) is list:
                for name in key:
                    try:
                        data.append(re.findall('\n{}=(.*)'.format(name), appdata)[0])
                        break
                    except IndexError:
                        if name in ['Name', 'Comment']:
                            data.append('')
            else:
                try:
                    data.append(re.findall('\n{}=(.*)'.format(key), appdata)[0])
                except IndexError:
                    data.append('')
        if data[4] == 'true':
            data[2] = '{} -T "{}" -e "{}"'.format(term, data[0], data[2])
        del data[4]
        alldata.append(data)
    alldata.sort(key=lambda elem: elem[0].lower())
    return alldata


def viewer(self, name, type):
    def close(button):
        dialog.destroy()

    def viewer_save(button):
        buf = text_view.get_buffer()
        text = buf.get_text(buf.get_start_iter(), buf.get_end_iter(), True)

        with open(path, "w+") as f:
            f.write(text)
    if type in ('edit', 'no-edit'):
        path = name
        with open(name) as f:
            name = f.read()
    dialog = Gtk.Dialog('Viewer', self, 0)
    dialog.set_default_size(750, 550)
    mainbox = dialog.get_content_area()
    mainbox.set_spacing(10)
    mainbox.set_border_width(10)
    title = Gtk.Label()
    if type == 'edit':
        title.set_markup('<big><b>JWM Kit Viewer</b></big>\nbasic functions only\n')
    row = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
    row.set_border_width(10)
    row.pack_start(title, False, False, 0)
    mainbox.pack_start(row, False, False, 0)
    scrolledwindow = Gtk.ScrolledWindow()
    text_view = Gtk.TextView()
    text = text_view.get_buffer()
    text.set_text(name)
    scrolledwindow.add(text_view)
    mainbox.pack_start(scrolledwindow, True, True, 0)
    row = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
    if type == 'edit':
        close_button = Gtk.Button(label='Close', image=Gtk.Image(stock=Gtk.STOCK_CANCEL))
        close_button.set_always_show_image(True)
        close_button.set_property("width-request", 90)
        close_button.connect("clicked", close)
        row.pack_end(close_button, False, False, 0)

        save_button = Gtk.Button(label='Save', image=Gtk.Image(stock=Gtk.STOCK_SAVE))
        save_button.set_always_show_image(True)
        save_button.set_property("width-request", 90)
        save_button.connect("clicked", viewer_save)
        row.pack_end(save_button, False, False, 0)

    else:
        ok_button = Gtk.Button(label='OK', image=Gtk.Image(stock=Gtk.STOCK_OK))
        ok_button.set_always_show_image(True)
        ok_button.set_property("width-request", 90)
        ok_button.connect("clicked", close)
        row.pack_end(ok_button, False, False, 0)

    if type != 'no-edit':
        config_button = Gtk.Button(label='Configure', image=Gtk.Image(stock=Gtk.STOCK_PROPERTIES))
        config_button.set_always_show_image(True)
        config_button.set_property("width-request", 90)
        config_button.connect("clicked", define_tools, self)
        row.pack_start(config_button, False, False, 0)
    mainbox.pack_start(row, False, False, 0)

    dialog.show_all()
    dialog.run()
    dialog.destroy()


class AppFail(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="JWM Kit First Run")
        try:
            self.set_icon_from_file('/usr/share/pixmaps/jwmkit/config.svg')
        except gi.repository.GLib.Error:
            self.set_icon_name('edit-paste')
        self.set_default_size(450, -1)
        self.set_border_width(20)
        main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.add(main_box)
        self.show_all()
        label = Gtk.Label()
        label.set_markup('<big><big><b>Welcome to JWM Kit</b></big></big>')
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=20)
        main_box.add(box)
        box.pack_start(Gtk.Image.new_from_file(filename='/usr/share/pixmaps/jwmkit/config.svg'), False, False, 0)
        box.pack_start(label, False, False, 0)
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=20)
        main_box.add(box)
        box.pack_start(Gtk.Label(), False, False, 0)
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=20)
        main_box.add(box)
        label = Gtk.Label()
        label.set_markup('<b> Unable to start the selected JWM Kit App.</b>')
        box.pack_start(label, False, False, 0)

        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=20)
        main_box.add(box)
        label = Gtk.Label(label="\tThe configuration needs to be initialized or repaired."
                                "\n\tJWM Kit will guide you through the process.")
        box.pack_start(label, False, False, 0)
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
        main_box.add(box)

        ok_button = Gtk.Button(label='OK', image=Gtk.Image(stock=Gtk.STOCK_OK))
        ok_button.set_always_show_image(True)
        ok_button.connect("clicked", ok_action)
        box.pack_end(ok_button, False, False, 20)


class AboutWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="About")
        self.set_icon_name('help-about')
        self.set_resizable(False)
        self.set_border_width(10)
        self.set_default_size(551, 370)
        main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=5)
        button_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
        self.add(main_box)
        close_button = Gtk.Button(label='OK', image=Gtk.Image(stock=Gtk.STOCK_OK))
        close_button.set_always_show_image(True)
        close_button.set_property("width-request", 90)
        close_button.connect("clicked", self.on_close)
        button_box.pack_end(close_button, False, False, 0)
        text_view = Gtk.TextView()
        text_view.set_editable(False)
        text_view.set_cursor_visible(False)
        text_view.set_left_margin(20)
        text_view.set_right_margin(20)
        text = text_view.get_buffer()
        text.set_text("\nJWM Kit - A set of Graphical Apps to simplify use of JWM (Joe's Window Manager)\n"
                      "Copyright © 2020-2022 Calvin Kent McNabb <apps.jwmkit@gmail.com>\n\n"
                      "This file is part of JWM Kit.\n\n"
                      "JWM Kit is free software; you can redistribute it and/or modify\n"
                      "it under the terms of the GNU General Public License, version 2,\n"
                      "as published by the Free Software Foundation.\n\n"
                      "JWM Kit is distributed in the hope that it will be useful,\n"
                      "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
                      "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
                      "GNU General Public License for more details.\n\n"
                      "You should have received a copy of the GNU General Public License\n"
                      "along with JWM Kit.  If not, see <https://www.gnu.org/licenses/>.\n")
        main_box.add(text_view)
        main_box.add(button_box)

    def on_close(self, button):
        self.destroy()
        Gtk.main_quit()

# ------ NOTES --------------------------------------------------------------------------------------------------------
# ------ Description of Functions and Classes -------------------------------------------------------------------------
# AboutWindow()  : Display JWM Kit License info
# get_about()    : Run the AboutWindow() - For button press
# txt_viewer(path) : Display the contents of a file
# ok_action()    : Run JWMKit FirstRun - For button press
# create_image() : Create an pixbuff and add to image. specify path, height, width, preserve aspect(True/False)
# AppFail()      : Error Dialog if JWM config is missing/corrupt
# warning_settings()      : Error Dialog if the JWMKit settings file is missing/corrupt
# get_applications_data() : Gather data from freedesktop files to make a list of installed apps.
# get_paths()    : Get all Includes in jwmrc
# find_configs() : Return a list of the config types corresponding to list returned from get_paths()
# id_path()      : Identify the config of the requested file. Used by find_config()
# theme_test()   : test if a file is a JWM Theme or not.
# find_terminal(): Attempt to identify the default terminal
# get_su()       : Read settings file for user's preference of permission request
# read_setting_su : return a list with with sudo settings, txt editor, archiver, etc
# define_tools   : Dialog for configuring various tools : editor, archiver, file manager
#
# private_entry(): Password Dialog
# get_jwmrc      : Determine default location of jwmrc based on version and configuration


# TODO
#   Move the following functions into this files :
#  - file picker
#  - get path from settings (startup, groups, keys, icons, preferences, theme, menu/tray history)
#  - get_icon_paths
#  - get_icon_dir
#  - update_icon
