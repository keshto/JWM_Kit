﻿| [Updates](#updates) | 
[Dependencies](#dependencies) | 
[Downloads](#downloads) | 
[Features](#some-of-jwm-kit-s-features) | 
[Tools](#summary-of-jwm-kit-s-tools) | 
[Behavior](#behavior-that-may-or-may-not-be-obvious) |
[Validation](#validation-check) |
[Wiki](https://codeberg.org/JWMKit/JWM_Kit/wiki) |

## JWM Kit

**A set of software designed to simplify usage of JWM (Joe's Window Manager)**  

JWM Kit is Free Open Source Software.  
See [License](https://codeberg.org/JWMKit/JWM_Kit/src/branch/master/doc/LICENSE).  
 
**Additional Projects**  
 [**SoundBraid**](https://codeberg.org/JWMKit/soundbraid) A simple sound mixer featuring support for sndio  
 ***

### UPDATES

**January 21 2022**

In General

- Code clean up
- Improved comments
- Remove developer notes (comments, TODO, etc) that were no longer needed
- Convert use of some class/functions to equivalent ones that have proven to be more optimal
- Update man pages and other documentation
- Resumed work on the previous suspended [JWM Kit Designer](https://codeberg.org/JWMKit/JWM_Kit/wiki/design).  A tool for creating and editing JWM Themes

Repair &amp; Restore

- Automatically create the JWM Kit Settings file on startup if it is missing
- Automatically open the repair_settings dialog when creating the settings file
- BugFix : installing a restore point could fail when converting the default config location

**January 18 2022**

- Built in viewer for text files and archive contents
- Dialog to configure externals tools for JWM Kit's use  
Example :  text editor, archiver and file manager
- The features improve the out of box experience and should be well worth the slight addition of code size.

**January 17 2022**  - Second Entry

JWM Kit Repair &amp; Restore improvements 

- Resolve compatibility issues when installing a restore point created for different versions of JWM. 
- Restore points can be shared between machines using different versions of JWM
- Updated WIki to include info on new JWM Kit Keys.

**January 17 2022**  
Some INFO and clarification

- JWM 2.4 supported but NOT required by JWM Kit.  
 JWM Kit still supports older version of JWM as well as the new version 2.4.  You can use either.
- As some have speculated a forth release is coming soon.  If you are packaging JWM Kit for redistributing you can save yourself some time by wait for the new upcomming release.

**January 16 2022**  
Complete re-write of JWM Kit Keys

- 40% less code
- Lighter on resources
- Major interface improvements
- Easier to use
- Support for key, keycode, and mouse bindings

**January 13 2022**  
New Stable Release 20220113


**January 12 2022**  
Better late than never so "Happy 2022. I hope everyone had a very Merry Christmas."

- JWM Kit now support both HOME/.jwmrc and HOME/.config/jwm/jwmrc  
This is in support of the new JWM 2.4. 
- If both files are on your system and JWM is version 2.4 or above, JWM Kit will used the new .config/jwm/jwmrc over the old one. This is to be consistent with the behavior observed when testing JWM 2.4
- Reduction of total code size.

**October 24 2021**  
Many people have asked about JWM themes and buttons sets to use with JWM and JWM KIt.  I have add a collection to sourceforge. The collection includes official JWM Kit themes and button sets as well as many themes and button sets from the Puppy Linux community. 

You can preview and download individual themes or button set from either the JWM Kit *wiki* or *sourceforge*. An option is provided to download all themes or all buttons sets as a .zip archive.

- [JWM Kit Wiki](https://codeberg.org/JWMKit/JWM_Kit/wiki#support-files)   - Look in the "Support Files" section
- [JWM Kit's sourceforge](https://sourceforge.net/projects/jwmkit/files/) - Look in the "themes" and " theme_button_sets" directories  
- Download All Themes :  [all_themes.zip](https://sourceforge.net/projects/jwmkit/files/themes/all_themes.zip/download)
- Download All Button Sets :  [all\_buttons.zip](https://sourceforge.net/projects/jwmkit/files/theme_button_sets/all_buttons.zip/download) 

**October 05 2021**

- Expanded Pop Volume's sndio support
- Provide a enhanced mixer for sndio
- sndio users can now control the level of each stream providing individual volume control for media player, web browser, etc. 
- sndio users with alsa-utils installed have the ability to control both the alsa and sindo master.

**October 2 2021** 

- JWM Kit Icons now shows a preview icon when selecting a search result.

**October 01 2021** - Second Entry

- New option for JWM Kit Logout : doas with no password
- The [JWM Kit Forum](https://sourceforge.net/p/jwmkit/discussion/) is open. Join the discussion!  
Discussion in other public forums will continue, but this forum will provide more freedom for discussion of JWM Kit

**October 01 2021**  

- JWM Kit Pop Volume now support sndio
- - sndioctl must be present
- - No configuration or parameters required to select sndio
- - Sndio is used when ALSA's amixer is not available.
- Improvements for ALSA support 
- Optional Dependencies
- - alsa-utils,  sndio 

**September 29 2021**  

- JWM Kit now has a [Wiki](https://codeberg.org/JWMKit/JWM_Kit/wiki). The wiki provides basic information on JWM Kit's various tools with plenty of pictures.

**September 24 2021**  

- Initial Stable Release is finally stable No more changes.
- Again moved release tag to latest commit. Sorry but it was necessary
- Fixed Debian's changelog and packaging scripts in BUILD
- Moved release tag to latest commit to include the above fixes in the initial stable release

**September 23 2021**  

- Finally!, A stable release :  [Initial Stable Release](https://codeberg.org/JWMKit/JWM_Kit/releases) 
- New packages and build scripts will be available on sourceforge soon.


**September 8 2021**  

- Add permission button to jwmkit_first_run which provides the ablity to configure how JWM Kit request permission.  
- Added the configuration file ~/.config/jwmkit/setting_su to record's the users preferences on how permission is requested.  
- Added a built in graphical password prompt to be used with sudo  

**September 8 2021**

- install packages for various distos moved to [Sourceforge](https://sourceforge.net/projects/jwmkit/files/Packages/)   
- A cleanup/restructuring of this Git (codeberg) is underway.  Sorry if this breaks any hyperlinks, bookmarks, scripts, etc  
- Preview images moved to sourceforge and google drive to reduce the tarball size of tagged releases
- Appologies to [vitforlinux](https://vitforlinux.wordpress.com/) as my recent changes will break the links in your article.  Thanks for the review.

---
### Dependencies *

Required

* python3
* python3-gi
* gir1.2-gtk-3.0  

optional (for pop up volume control / notification)

* alsa-utils
* sndio \*\*

*This list of dependencies uses Debian based packages name.  
Equivalent package may have a different name on non-Debian based distributions.  
 \*\* Not recommended for Debian based system because the package is way out of date. If wish to use sndio on Debian you probally need to compile it 
 
---
### DOWNLOADS

| Download | Description |
|--|--|
|  [DEB ](https://sourceforge.net/projects/jwmkit/files/Packages/Debian/)   | packages for Debian, Devuan, Ubuntu, etc |
|  [Arch ](https://sourceforge.net/projects/jwmkit/files/Packages/Arch/) | Packages for Arch, Manjaro, etc|
| [Puppy](https://sourceforge.net/projects/jwmkit/files/Packages/Puppy/)  | Packages for FOSSA & SLACKO Puppy Linux |
| [Build Script](https://sourceforge.net/projects/jwmkit/files/BUILD/) | make your own package |
|  [JWM Kit Linux](https://sourceforge.net/projects/jwmkit/files/iso/)  | Evaluate  JWM Kit in a live boot enviroment |

---
### Some of JWM Kit's Features

* No part of this software is designed to runs in the background. It only runs when the user starts it, and properly quits when "closed", or for notifications will close when timed out in a few seconds.
* Features are implemented using JWM's existing abilities.
* A settings manager as seen in popular desktops
* Set the desktop background as an image, solid color or gradient.
* configure icon paths
* Preview and Switch JWM Themes
* Preview and Switch JWM button sets (window buttons like: close, max, min, etc)
* Create a new button set by replacing the colors on an existing set (SVG)
* Configure the appearance and behavior of JWM trays, and menus.
* Add & Remove JWM trays, and menus
* Add & Remove items to trays, and menus
* Configure commands (scripts, apps, etc) to run when JWM starts, restarts and/or shutdown
* Save current Screen configuration for startup (resolution/frame rate/monitor position/etc)
* Configure key board shortcuts for custom controls, and/or quick access
* add a simple popup calendar to the tray to the clock (or icon/key shortcut)
* add a battery monitor to a tray icon or keyboard shortcuts
* add volume notification and control to an tray icon or keyboard shortcuts
* Specify options for a group of programs by name or class
* Create a restore point of the JWM configuration
* Restore the JWM configuration to an earlier time/date
* Receive feedback useful for finding and repairing the JWM Configuration

As they say a picture is worth a thousand words. Previews are avalible at the following locations:

- [Google Drive](https://drive.google.com/drive/folders/1j4nRrMzOq_kXwCBDyKIgVFnuKRZYRg_U?usp=sharing) 
- [Sourceforge](https://sourceforge.net/projects/jwmkit/files/preview_images/ )  
- The [Wiki](https://codeberg.org/JWMKit/JWM_Kit/wiki) also contains a good number of pictures  

---
### Summary of JWM Kit's tools  
You can Read more about these tools in the [wiki](https://codeberg.org/JWMKit/JWM_Kit/wiki#support-files) 

| Tool | Purpose |
|--|--|
|First Run|Assistance after the initial install of JWM Kit|
|Appearance|Preview and set JWM Theme, and/or button set|
|Menus|Configure the appearance and behavior of JWM menus
|Trays|Configure the appearance and behavior of JWM trays|
|Repair & Restore|Tools to repair the JWM configuration|
|Settings|A settings manager as seen in popular desktops|
|Freedesktop| Freedesktop entries  (Similar to Menulibe)|
|Easy Menu|Generate an Application Menu from freedesktop files|
|Calendar|pop up calendar for tray|
|Groups|Specify options for a group of programs by their name and/or class|
|Icon|Tell JWM where to find icons|
|Keys|Configure mouse and keyboard shortcuts|
|Logout|Simple Dialog with Logout, reboot, and shutdown options|
|Pop Volume|Assign volume control and notification to a tray icon or key shortcuts|
|Startups|Configure commands (scripts, apps, etc) to run when JWM starts, restarts and/or shutdown|
|Wallpaper|Set the background with an image or color(s)|
|Time|Set the System's time, date, and time zone|
|Battery Menu|Generate a JWM Menu that provides battery info|

---
#### Behavior that may or may not be obvious   
* Entries in JWM Kit Startups tool must have at least one option selected. Options are Startup, Restart, Shutdown. Items without an option selected will not be saved.  
* Reminder of the obvious: Although you may see your changes with in an Application, Changes are not written to disk until you press the save button.
* Comments are not preserved in the XML files.  While its not been well tested, JWM Kit seems to have no problem processing XML files with comments.   
* JWM Kit uses the $HOME variable to represent the users home directory. This allowing a config files to be used by another user.  

---
#### Validation check  
JWM Kit does check the config files for proper form and syntax.  Sometimes it may check a file for it's intended purpose. While these checks are helpful, they are limited.  JWM Kit maybe unable to use a file If it was improperly edit either manually or by another program.
