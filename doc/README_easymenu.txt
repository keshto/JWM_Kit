######### JWM Kit Easy Menu  ###################################################
######### Parameters   #########################################################

Usage:	jwmkit_easy_menu <parameter>:[option]:[option]       
Note:	use as many parameter as needed.

################################################################################
#########  Reminder / tips  ####################################################

- Enclose a value with quotes ( " ) if the value has problem characters
  Spaces and colons ( : ) are the main problem character
- If needed place the entire Parameter in quotes
  Example "-hc:JWM Kit" or just the option example: -hc:"JWM Kit"
- Instead of quotes, escape the character example: -hc:JWM\ Kit
- Escape a quote if the prameter is inside quotes
  Example: "-ha:myapp --loadfile=\"/path/to a/files\""
  In this example it would be easier to not use quotes and escape the spaces

################################################################################
#########  Syntax  #############################################################

-static		!!! Experimental !!! Incomplete !!! 
		Create a static menu for use and ( TODO ) write it to file  
		# Missing attrib options! height/label/etc
option		Rootmenu's "onroot" mousebutton/letter shortcut(s)
Example		-static:13 will create a menu using onroot="13" (Lt & Rt mouse)
---------------
-hc		[H]ide [C]ategory, Remove the whole category from output
option		category or categories to remove. separate entries with :
Example		-hc:System		remove System menu for output
Example		-hc:System:Settings	remove both System & settings menu
---------------
-ha		[H]ide [A]pplication	Separate entries with :
option		Command of app to be removed.  Case insensitive
		As it appears in the exec= line of the .desktop file
Example		-ha:firefox    Remove Firefox from the menu
Example		-ha:firefox:chrome  Remove both Firefox & chrome from the menu
Note		on rare occasion you may need to specify more than one command
Example		-ha:firefox:/usr/lib/firefox/firefox
Note		Since colons are part of the syntax, You must escape them
Note		escape colons : this way \\:\\   Example "-ha:firefox https\\:\\//www.site.com"
---------------
-l		[L]anguage: specify a preferred [l]anguage
option		LANGUAGE standard unix language codes
Example		-l:en       # English
---------------
-lt		[L]anguage / [T]erritory: specify a preferred language and territory
option		LANGUAGE_TERRITORY standard unix language code and territory code
Example		-lt:en_AU    # Australian English
---------------
-t		[T]erminal specify the terminal to use with terminal apps
option		command to launch your preferred terminal
Example		jwmkit_easy_menu -t:lxterminal
---------------
-r		[R]ename a category.
option 1	Original category name
option 2	New category name
Example:	jwmkit_easy_menu -r:Accessories:Utilities
---------------
-m		[M]erge categories
option 1	Receiving Category (Category to be displayed)
option 2	Category to hide show contents in option 1 category
option 3	additional category to hide & show contents in option 1 category
option 4	sames as 2 & 3 add as many as you want. They will all be merged
Example		jwmkit_easy_menu -m:Education:Science
		---Science items in the Education menu
Example		jwmkit_easy_menu -m:Education:Science:Development
		---Includes Science & Development items in Education

----- Parameters with no options -----

-nodubs		Remove Duplicates. Limit apps to a single category.
-nokit		same as "-hc:JWM Kit" hide JWM Kit items (except Settings Panel)
-nokitmenu	Do not create a JWM Kit menu folder.
		JWM Kit App will be placed in the Settings menu

################################################################################
#########  Example  ############################################################

----------Desired Output -------------------------------- Parameter / Options
Rename the Multimedia menu to AudioVideo		-r:Multimedia:AudioVideo
No duplicate items in our menu:				-nodubs
merge the Science menu into the Education menu		-m:Education:Science
Rename Education to EduSci				-r:Education:Edu-Sci

Put it all together like this:
jwmkit_easy_menu -r:Multimedia:AudioVideo -nodubs -m:Education:Science -r:Education:Edu-Sci



################################################################################
######### Run this tool without jwmkit installed ###############################
#
# To create a 'stand alone' version of this tool
#
# 1. Copy the find_terminal() function from the file jwmkit_utils and paste it directly below this info section
# 2. Scroll up to line 4 and delete the line "import jwmkit_utils"
# 3. Scroll down and find the line that looks like this:
#       term = jwmkit_utils.find_terminal()
#     Now remove the work "jwmkit_utils" so the line now looks like this:
#       term = find_terminal()
#
# ############### INFO #########################################################
