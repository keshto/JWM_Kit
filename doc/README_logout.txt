######### IMPORTANT NOTICE #####################################################

You can use waste a lot of time reading this document and manually configuring
your system, or . . . 

Just type the following into a your terminal:
jwmkit_first_run
Then click the "Permissions" button and use select your options from a GUI.
Don't forget to press the 'save' button when done.
 

######### JWM Kit Logout  ######################################################

Two options to specify how JWM Kit Logout peforms reboot and shutdown.
 1. Specify a parameter when starting jwmkit_logout. example: jwmkit_logout nosu
 2. Specify the parameter in the file ~/.config/jwmkit/setting_su

Details of the file ~/.config/jwmkit/setting_su
 - The file configures jwmkit_logout & jwmkit_time (jwmkit_time not complete)
 - Example of setting_su file:   (replace sudop with your own choice)
	logout=sudop
	time=sudop
 - The tools defaults are used if the lines are missing, commented out or
   the file does not exist.

Notice : The setting_su file is ignored when starting jwmkit_logout with a parameter
Notice2: Restore points do not include settings_su.  It would not be wise to 
	 including a system specific config file in a restore point that can be
	 shared and used on different systems.

######### Recent Changes #######################################################

Sept 11 2021 : New Feature
    - Added the optional config file ~/.config/jwmkit/setting_su
Sept 10 2021 : New Features
    - Parameter to specify custom reboot and power off commands
Sept 5 2021 : New Features
    - Parameter to us a terminal emulator for password input
    - Added a built in authentication prompt for su and sudo
    - Parameter to use built in authentication prompt
    - Parameter for Systemd ( systemctl )
    - Parameter for elogind (loginctl)
    - Remove all prepending dashes when processesing parameter

######### Parameters   #########################################################

- Most Parameters use /sbin/reboot and /sbin/poweroff
- symbols
  *  More details provided below in Advanced Parameters section
  ** Does not use the default /sbin/poweroff and /sbin/reboot
  +  Means the option is not available for jwmkit_time

Parmeters term, sup, sudop, sysd, and elogind require a version > 20210910

default**+ (No Parameters)--used dbus with ConsoleKit **+
sudo   ---------------------sudo without password for permission
gksu_r ---------------------gksu ( real ) for permssion
gksu  ----------------------gksu ( fake ) for permission
gksudo_r -------------------gksudo ( real ) for permssion
gksudo  --------------------gksudo ( fake ) for permission
term* ----------------------specify a terminal and permission method *
custom* --------------------specify a custom command for poweroff and reboot * 
su* ------------------------use su ( terminal for password prompt ) *
sudop ----------------------use sudo with JWM Kit's built in graphic password prompt
sudoa ----------------------use sudo -A ( sudo --askpass )
sysd** ---------------------use systemd ( systemctl or timedatectl ) **
elogind**+------------------use elogind ( loginctl )  **+
x  -------------------------use su-to-root -X -c
nox* -----------------------use su-to-root -c ( terminal for password prompt )
puppy**+ -------------------use wmreboot found in puppy linux **+
nosu  ----------------------issue commands with out requesting permission

######### Alias Parameters   ###################################################


Alias parmeters have been retired to simply code. If your configuration uses an
alias parameter it will continue to work for now, but eventually this function
will be removed. Please update your configuration to use one of the parameters
listed above.


######### Advanced Parameters  (with examples) #################################

* custom : custom:[reboot command]:[shutdown command]
  - Example 1 : jwmkit_logout custom:~/sbin/reboot.sh:~/sbin/poweroff.sh
  - Example 2 : jwmkit_logout custom:gksu ~/sbin/reboot.sh:gksu ~/sbin/poweroff.sh

* term : term:[chosen terminal]:[choosen permission tool and it's parmaters]
  - Example 1 : jwmkit_logout term:xfce4-terminal:sudo
  - Example 2 : jwmkit_logout term:xterm:doas
  - Example 3 : jwmkit_logout "term:xterm:doas -u username"

* su : su:[chosen terminal]
  - Example   : jwmkit_logout su:lxterminal
    uses lxterminal to issue command su -c /sbin/poweroff (or reboot)

* nox : nox:[chosen terminal]
  - Example   : jwmkit_logout nox:xterm
    uses xterm to issue command su-to-root -c /sbin/poweroff (or reboot)


######### Examples   ###########################################################

jwmkit_logout ---------will use dbus with ConsoleKit
jwmkit_logout nosu ----will use /sbin/poweroff (or reboot)
jwmkit_logout term:lxterminal:sudo - use sudo and lxterminal for password input
jwmkit_logout custom:"/usr/local/reboot.sh":"/usr/local/poweroff.sh"

sudo jwmkit_logout nosu --This is kind of a hail mary command. By launching
	jwmkit_logout with sudo/doas/gksudo/etc you are giving jwmkit_logout
	root permission meaning it can peform a shutdown and reboot without
	configuring the system. While a useful and simple option, it is not
	an option you'd see recommended by a security expert. While the command
	is not forbidden there are better solution.


######### ADDITIONAL INFO  #####################################################
################################################################################

It seems to be necessary for me to expand on the capablities of this tool.

JWM Kit Logout has no secret function for gaining the permission it needs to
peform a poweroff or reboot. The parameters allow the user to select an option
compatible with their system, but it is the users responsiblity to ensure
their system is properly configured for the option selected.

----- Review of parmater requirements and how it gains permission -------------

default - Requires dbus and ConsoleKit to be installed and configured.
sudo	- Requires sudo to be installed and the sudoer file must be configured
	  to execute poweroff and reboot without a password
gksu	- Requires gksu or a "fake" gksu configuration
gksudo	- Requires gksudo or a "fake" gksudo configuration
su-to-root (all parmeters) must have a su-to-root installed and configured.
no sudo - Requires permission to execute poweroff and reboot.
	  How do you get this permission: 
		1. Run JWM Kit Logout as root
		2. Give poweroff and reboot execute permission
		3. set UID of poweroff and reboot
sysd	- Requires systemd
sudop	- Requires sudo be installed and the sudoer file must be configured
su	- Requires su and a terminal emulator. - pretty sure you have it ;)

NOTE :	gksu and gksudo have been depreciated by most major distros. You can
	use the built in sup and sudop instead.

Need help configuring one of these options?  Just search online. There is an
amazing amount of tutorials/advice/etc addressing these specific configurations
on trust worthy site like stackexchange, askubuntu, various linux forums.
