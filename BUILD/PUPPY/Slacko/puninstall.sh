#!/bin/bash
# Enable fixmenus
chmod +x /usr/sbin/fixmenus

# Restore JWM Config to preinstall status
if test -f "$HOME/.config/jwm/jwmrc"; then
  rm ~/.config/jwm/jwmrc
fi
cp -r /etc/jwmkit/.backup/jwm/ ~/.jwm/
cp /etc/jwmkit/.backup/.jwmrc ~/.jwmrc
cp /etc/jwmkit/.backup/.jwmrc-tray ~/.jwmrc-tray

# re-enable executables that conflict with JWM Kit
CONFLICTS=("/usr/sbin/ptheme" "/usr/sbin/pdesktop" "/usr/local/jwm_config/" "/usr/local/jwmconfig2/" "/usr/local/jwmdesk/")
for FILE in "${CONFLICTS[@]}"; do
  if test -f "$FILE"; then
    chmod +x "$FILE"
  elif test -d "$FILE"; then
    chmod -R +x "$FILE"
  fi
done

# Hide apps that were previously hidden
# Apps will be avalible via other apps
# HIDEAPP=("lxappearance" "conky-gtk" "dunst-config" "icon_switcher" "FontManager" "pcur" "pschedule" "ptheme_gtk"
#         "ptheme_roxpin" "jwm_theme_switcher" "jwm_theme_switcher" "ptheme_roxpin")
         
HIDEAPP=("lxappearance" "conky-gtk" "dunst-config" "icon_switcher" "FontManager" "pcur" "pschedule" "ptheme_gtk"
         "ptheme_roxpin" "ptheme_roxpin")

for DESK_FILE in "${HIDEAPP[@]}"; do
  if test -f "/usr/share/applications/${DESK_FILE}.desktop"; then
    if grep "NoDisplay=false" "/usr/share/applications/${DESK_FILE}.desktop"; then
      sed -i 's/NoDisplay=false/NoDisplay=true/' "/usr/share/applications/${DESK_FILE}.desktop"
    elif ! grep "NoDisplay=true" "/usr/share/applications/${DESK_FILE}.desktop"; then
      echo -e "\nNoDisplay=true" >> "/usr/share/applications/${DESK_FILE}.desktop"
    fi
  fi
done

# Show apps hidden due to conflicts
SHOWAPPS=("jwmdesk" "jwm_tray" "theme-toggle" "ptheme" "pdesktop" "Wizard-Wizard" "PupClockset")
for DESK_FILE in "${SHOWAPPS[@]}"; do
  if test -f "/usr/share/applications/${DESK_FILE}.desktop"; then
    if grep "NoDisplay=true" "/usr/share/applications/${DESK_FILE}.desktop"; then
      sed -i 's/NoDisplay=true/NoDisplay=false/' "/usr/share/applications/${DESK_FILE}.desktop"
    fi
  fi
done

# have puppy's fixmenus function rebuild the menus to the preinstall configuration
/usr/sbin/fixmenus
jwm -restart
pid=$(pgrep -f "gtkdialog -p PPM_GUI")
kill -s TERM $pid
