#!/bin/bash

yad_post_confirm() {
    if test -f "/usr/share/jwmkit/.backup/templates/_root_.jwmrc"; then
    FIELD_VALUES="false false false false false"
  else
    FIELD_VALUES="true true true true true"
  fi
  MESSAGE1="<b>Configure System for JWM Kit</b>\n\n<b>Recommendations:</b>\n\t* <i>Clean Install</i> "
  MESSAGE2="(JWM Kit was not installed) - Check all\n\t* <i>Update</i> - Check None unless you are having problems\n"
  yad --form --separator=' ' \
    --title="Configure System" \
    --window-icon=/usr/share/pixmaps/jwmkit/config.png \
    --field "$MESSAGE1$MESSAGE2":lbl \
    --field "Overwrite JWM Configuration with the JWM Kit default\n* For Clean installs, not advised on upgrade unless your jwm config is damaged":chk \
    --field "Hide apps that depend on Puppy's default config\n* In almost all cases the same function is provided by JWM Kit":chk \
    --field "Disable apps that have errors without Puppy's default config\n* In almost all cases the same function is provided by JWM Kit":chk \
    --field "Show apps hidden by default. Useful apps normally accessible\n* as part of the apps hidden or disabled by the above options.":chk \
    --field "Install Dependencies":chk \
    --field "\n<b>Note:</b> A backup of the pre-installed config was made.\nuninstalling JWM Kit will restore your system configuration\nAs long as the backup still exist.":lbl \
    "" $FIELD_VALUES
}

yad_progress() {
  yad --progress \
    --title="Installing Dependencies." \
    --width=350 \
    --height=100 \
    --borders=20 \
    --no-escape \
    --text-align=center \
    --text="JWM Kit will be ready soon and JWM will be reloaded" \
    --pulsate \
    --auto-close \
    --no-buttons
}

install_depends() {
  pkg repo-update
  # Ubuntu based Puppy
  pkg --get python3-gi gir1.2-gtk-3
  # Slakware based Puppy
  # pkg --get python3 pygobject3-python3

}

fix_buttons(){
	# Give JWM Kit access to Puppy's button_sets
	for DIR in /usr/local/jwmdesk/jwm_button_themes/*/; do
		BNAME=$(basename "$DIR")
		BNAME=/root/.config/jwm/themes/buttons/"$BNAME"
		if [[ ! -e "$BNAME" ]] && [[ -d "$DIR" ]]; then
			cp -r "$DIR" "/root/.config/jwm/themes/buttons/"
		fi
done
}

fix_themes(){
    # Give JWM Kit access to Puppy's JWM Themes
    # repair known errors in Puppy's themes.
	for FILE in /root/.jwm/themes/*jwmrc; do
    BNAME=$(basename "$FILE")
    BNAME=/root/.config/jwm/themes/"$BNAME"
    if grep "Style>" "$FILE" > /dev/null && [[ ! -e "$BNAME" ]]; then
        sed -i 's/<?xml version.*/<?xml version="1.0"?>/' "$FILE"
        sed -i 's/>>/>/g' "$FILE"
        sed -i 's/<\/Foreground$/<\/Foreground>/g' "$FILE"
        cp "$FILE" "$BNAME"
    fi
done
}

disable_conflicts(){
  # disable apps that conflict with JWM Kit
  # These apps are only useful for a standard puppy configuration
  # and in most cases the same function is provided by JWM Kit
  CONFLICTS=("/usr/sbin/ptheme" "/usr/sbin/pdesktop" "/usr/local/jwm_config/" "/usr/local/jwmconfig2/" "/usr/local/jwmdesk/")
  for FILE in "${CONFLICTS[@]}"; do
    if test -f "$FILE"; then
      chmod -x "$FILE"
    elif test -d "$FILE"; then
      chmod -R -x "$FILE"
      #test -f "${FILE}theme_switcher" && chmod +x "${FILE}theme_switcher"
      test -f "${FILE}file-selector" && chmod +x "${FILE}file-selector"
    fi
  done
}

hide_conflicts(){
  # These apps will be hidden To prevent confusing the user as the app shows no results when using JWM Kit's config
  # in most cases the same function is provided by JWM Kit
  HIDEAPP=("jwmdesk" "jwm_tray" "theme-toggle" "ptheme" "pdesktop" "Wizard-Wizard" "PupClockset")
  for DESK_FILE in "${HIDEAPP[@]}"; do
    if test -f "/usr/share/applications/${DESK_FILE}.desktop"; then
      if grep "NoDisplay=false" "/usr/share/applications/${DESK_FILE}.desktop"; then
        sed -i 's/NoDisplay=false/NoDisplay=true/' "/usr/share/applications/${DESK_FILE}.desktop"
      elif ! grep "NoDisplay=true" "/usr/share/applications/${DESK_FILE}.desktop"; then
        echo -e "\nNoDisplay=true" >> "/usr/share/applications/${DESK_FILE}.desktop"
      fi
    fi
  done
}

show_hidden(){

  make_desktop() {
    if ! test -f "$DESK_NAME"; then
      echo -e "[Desktop Entry]\nEncoding=UTF-8\nType=Application\nName=${DESK_NAME}\nIcon=${DESK_ICON}" > "$DESK_FILE"
      echo -e "Comment=${DESK_NAME}\nExec=${DESK_EXE}\nCategories=X-Desktop-appearance\nGenericName=${DESK_NAME}" >> "$DESK_FILE"
    fi
  }

  # Puppy hides the following apps by default and presents them as part of one of the apps we hide above.
  # Since they do work well with JWM Kit it is best to un-hide them.
  SHOWAPPS=("lxappearance" "conky-gtk" "dunst-config" "icon_switcher" "FontManager" "pcur" "pschedule" "ptheme_gtk")
  for DESK_FILE in "${SHOWAPPS[@]}"; do
    if test -f "/usr/share/applications/${DESK_FILE}.desktop"; then
      if grep "NoDisplay=true" "/usr/share/applications/${DESK_FILE}.desktop"; then
        sed -i 's/NoDisplay=true/NoDisplay=false/' "/usr/share/applications/${DESK_FILE}.desktop"
      fi
    fi
  done

  # More hidden apps that work well with JWM Kit, and need a freedesktop file to be created
  # FILES=("/usr/sbin/ptheme_roxpin" "/usr/local/jwm_config/theme_switcher" "/usr/local/jwmdesk/theme_switcher" "/usr/local/desksetup/desksetup.sh")
  #  DESK_NAMES=("ROX Desktop Icons" "JWM Theme Switcher" "JWM Theme Switcher" "ROX Desktop Icons")
  #  DESK_FILES=("ptheme_roxpin" "jwm_theme_switcher" "jwm_theme_switcher" "ptheme_roxpin")
  FILES=("/usr/sbin/ptheme_roxpin" "/usr/local/desksetup/desksetup.sh")
  DESK_ICON="/usr/share/pixmaps/puppy/windows_theme.svg"
  DESK_NAMES=("ROX Desktop Icons" "ROX Desktop Icons")
  DESK_FILES=("ptheme_roxpin" "ptheme_roxpin")
  LEN=${#FILES[@]}
  for (( i=0; i<$LEN; i++ )); do
    if test -f "${FILES[$i]}"; then
      DESK_NAME=${DESK_NAMES[$i]}
      DESK_EXE=${FILES[$i]}
      DESK_FILE="/usr/share/applications/${DESK_FILES[$i]}.desktop"
      make_desktop
    fi
  done
}

replace_config(){
  # replace default configs with JWM Kit configs
  # Backup default puppy config only if no backup
  if ! test -f "/etc/jwmkit/.backup/.jwmrc"; then
    mkdir -p /etc/jwmkit/.backup/jwm/themes/buttons
    cp -r ~/.jwm/ /etc/jwmkit/.backup/jwm/
    cp ~/.jwmrc /etc/jwmkit/.backup/
    #mv /etc/xdg/templates/* /etc/jwmkit/.backup/templates/
    #cp /etc/jwmkit/.backup/templates/README.txt /etc/xdg/templates/README.txt
    if test -f ~/.jwmrc-tray; then
      cp ~/.jwmrc-tray /etc/jwmkit/.backup/.jwmrc-tray
    fi

  fi
  mkdir -p ~/.config/jwm/themes/buttons ~/.config/jwmkit

  # The function of the following lines is replaced with a jwmkit_repair restore 
  # cp /etc/jwmkit/.jwmrc ~/.jwmrc
  # cp /etc/jwmkit/settings ~/.config/jwmkit/settings
  # cp -r /etc/jwmkit/jwm/ ~/.config/
  jwmkit_repair --restore_default
}

# disable fixmenus until configuration is complete
chmod -x /usr/sbin/fixmenus

# Set category in freedesktop file. Prevents having to maintain a "puppy version" of these files
# and also overwrites whatever random category ppm decides to use.
DESKTOPS=("JWMKit-Appearance" "JWMKit-Groups" "JWMKit-Icons" "JWMKit-Keys" "JWMKit-Menus"
          "JWMKit-Startups" "JWMKit-Trays" "JWMKit-Wallpaper" "JWMKit-Repair-Restore")
          
for DESK_FILE in "${DESKTOPS[@]}"; do
  sed -i 's:Categories=.*:Categories=X-Desktop':g "/usr/share/applications/${DESK_FILE}.desktop"
done
sed -i 's:Categories=.*:Categories=System':g "/usr/share/applications/JWMKit-Settings.desktop"

confirm=$(yad_post_confirm)
for RESPONSE in $(echo "$confirm" | xargs); do
  RESPONSES+=("$RESPONSE")
done


if [ "${RESPONSES[1]}" == "TRUE" ]; then
  hide_conflicts
fi

if [ "${RESPONSES[2]}" == "TRUE" ]; then
  disable_conflicts
fi

if [ "${RESPONSES[3]}" == "TRUE" ]; then
  show_hidden
fi

if [ "${RESPONSES[4]}" == "TRUE" ]; then
  install_depends | yad_progress
fi

if [ "${RESPONSES[0]}" == "TRUE" ]; then
  replace_config
fi

# import puppy's default themes and buttons to JWM Kit's path
fix_buttons
fix_themes

jwm -restart
